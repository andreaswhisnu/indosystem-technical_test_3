mohon maaf saya tidak sempat selesaikan semua jawabannya dalam waktu 3 jam.
saat ini ada beberapa pertanyaan dari soal kedua yang belum saya selesaikan dan aplikasi untuk soal ketiga juga belum dapat saya selesaikan dengan baik.

Cara Menjalankan Aplikasi:
- clone repository https://gitlab.com/andreaswhisnu/indosystem-technical_test_3.git
- jalankan composer update untuk install library yang saya gunakan
- buat database dan setting environment (.env) database
- jalankan php artisan migrate:refresh --seed
- pastikan folder /storage dan /bootstrap writeable oleh web service php
- buka aplikasi dari browser
- untuk login admin gunakan email superadmin@guestbook.co dan password adminadmin.
