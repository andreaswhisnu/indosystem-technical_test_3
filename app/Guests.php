<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guests extends Model
{
	use SoftDeletes;

    protected $table = 'guests';
	protected $primaryKey = 'idguests';

	protected $fillable = [
        'name', 'address', 'phone', 'note'
    ];
}
