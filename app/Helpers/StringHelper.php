<?php

//SQL Injection
function sanitize_string($string)
{
    return preg_replace("/[~`!#$%^&*()?+=<>]+/", "", $string);
}