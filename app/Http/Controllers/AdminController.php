<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guests;
use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admincheck')->except('login','login_do');
    }

    public function index()
    {
    	$data['guests'] = Guests::paginate(10);

    	return view('admin_index', $data);
    }

    public function delete(Guests $guest)
    {
        //delete guest
        $guest->delete();

        return redirect()->to('/admin');
    }

    public function login()
    {
    	return view('admin_login');
    }

    public function login_do(Request $request)
    {
    	//validator
        $this->validate($request, [
            'email' => 'required|email|min:8|max:255',
            'password' => 'required|min:8|max:255'
        ]);

        //filter input
        $email = sanitize_string($request->email);
        $password = sanitize_string($request->password);

        //authentication
        if(Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->to('/admin');
        }
        else {
            return redirect()->to('/admin/login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('/admin/login');
    }
}
