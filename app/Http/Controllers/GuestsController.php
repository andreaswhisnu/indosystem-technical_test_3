<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guests;

class GuestsController extends Controller
{
    public function form_input()
    {
    	return view('guest_form');
    }

    public function form_save(Request $request)
    {
    	//validator
        $this->validate($request, [
		    'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|numeric|digits_between:6,20',
            'note' => 'required|max:255'
		]);

        //filter input
        $fillable = array(
            'name' => sanitize_string($request->name),
            'address' => sanitize_string($request->address),
            'phone' => sanitize_string($request->phone),
            'note' => sanitize_string($request->note),
        );

		//save guest form
		$guest = new Guests;
		$guest->fill($fillable);
		$guest->save();

        return redirect()->to('/guest/form');
    }
}
