<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guest Book Form :: Indo System</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet">

  </head>
  <body>

    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<h2 class="text-center">Guest List</h2>
						<div class="pull-left">
							<a href="{{url('guest/form')}}">Guest Form</a>
						</div>
						<div class="pull-right">
							<?php $user = Auth::user()?>
							<span>Halo, {{$user->name}} :: </span>
							<a href="{{url('admin/logout')}}">Logout</a>
						</div>
						<br><br>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Address</th>
								<th>Phone Number</th>
								<th>Note</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@foreach($guests as $num => $data)
							<tr>
								<td>{{$data->idguests}}</td>
								<td>{{$data->name}}</td>
								<td>{{$data->address}}</td>
								<td>{{$data->phone}}</td>
								<td>{{$data->note}}</td>
								<td>
									<center>
						                <a href="{{ url('/admin/delete/'.$data->idguests) }}">delete</a>
						            </center>
								</td>
							</tr>
							@endforeach
						</tbody>
						</table>
						{{$guests->links()}}
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="{{url('assets/js/jquery.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/scripts.js')}}"></script>
  </body>
</html>