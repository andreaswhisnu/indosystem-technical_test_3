<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Login - Guest Book :: Indo System</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet">

  </head>
  <body>

    <div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="row">
					<div class="col-md-12">
						<h2 class="text-center">Admin Login</h2>
						<a href="{{url('guest/form')}}" class="pull-right">Guest Form</a>
						<br><br>
					</div>
				</div>

				@if($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif

				{{ Form::open(array('url' => 'admin/login-do', 'role' => 'form'))}} 
				<div class="form-group">
					<label for="guestInputName">Email</label>
					<input type="email" class="form-control" name="email" id="exampleInputEmail" placeholder="Email" required>
				</div>
				<div class="form-group">
					<label for="guestInputAddress">Password</label>
					<input type="password" class="form-control" name="password" id="exampleInputPassword" placeholder="Password" required>
				</div>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-primary">Submit</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>

    <script src="{{url('assets/js/jquery.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/scripts.js')}}"></script>
  </body>
</html>