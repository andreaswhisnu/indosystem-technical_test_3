<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guest Book Form :: Indo System</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet">

  </head>
  <body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="text-center">
                    <h2>Page Not Found</h2>
                    <label>
                        Go to <a href="{{url('guest/form')}}">Guest Form</a><br>
                        or go to <a href="{{url('admin')}}">Admin Page</a> if you are administrator
                    </label>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('assets/js/jquery.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/scripts.js')}}"></script>
  </body>
</html>