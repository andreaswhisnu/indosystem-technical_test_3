<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guest Book Index :: Indo System</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet">

  </head>
  <body>

    <div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="row">
					<div class="col-md-12">
						<h2 class="text-center">Guest Form</h2>
						<h4 class="text-center">Input Guest Data</h4>
						<a href="{{url('admin')}}" class="pull-right">Admin Page</a>
						<br><br>
					</div>
				</div>

				@if($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif

				{{ Form::open(array('url' => 'guest/form-save', 'role' => 'form'))}} 
				<div class="form-group">
					<label for="guestInputName">Name</label>
					<input type="text" class="form-control" name="name" id="exampleInputName" placeholder="Name" required>
				</div>
				<div class="form-group">
					<label for="guestInputAddress">Address</label>
					<input type="text" class="form-control" name="address" id="exampleInputAddress" placeholder="Address" required>
				</div>
				<div class="form-group">
					<label for="guestInputPhone">Phone</label>
					<input type="text" class="form-control" name="phone" id="guestInputPhone" placeholder="Phone Number" required>
				</div>
				<div class="form-group">
					<label for="guestInputNote">Note</label>
					<input type="text" class="form-control" name="note" id="guestInputNote" placeholder="Note" required>
				</div>
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-primary">Submit</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>

    <script src="{{url('assets/js/jquery.min.js')}}"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/scripts.js')}}"></script>
  </body>
</html>