<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('guest/form');
});

Route::get('guest', 'GuestsController@index');
Route::get('guest/form', 'GuestsController@form_input');
Route::post('guest/form-save', 'GuestsController@form_save');

Route::get('/admin', function () {
    return redirect('admin/index');
});
Route::get('admin/index', 'AdminController@index');
Route::get('admin/delete/{guest}', 'AdminController@delete');
Route::get('admin/login', 'AdminController@login');
Route::post('admin/login-do', 'AdminController@login_do');
Route::get('admin/logout', 'AdminController@logout');